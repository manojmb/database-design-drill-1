CREATE DATABASE branch;

-- Use Database branch
USE branch;

-- Book Table
CREATE TABLE Book
(
    ISBN INTEGER PRIMARY KEY,
    Title VARCHAR(255),
    Author VARCHAR(255),
    Publisher VARCHAR(255)
);

-- Branch Table
CREATE TABLE Branch
(
    BranchID INTEGER PRIMARY KEY,
    BranchAddr VARCHAR(255),
    BranchCity VARCHAR(255),
);


CREATE TABLE Bookcopies
(
    BranchID INTEGER PRIMARY KEY,
    ISBN INTEGER,
    NumCopies INTEGER,
    PRIMARY KEY (BranchID,ISBN),
    FOREIGN KEY (BranchID) REFERENCES Branch(BranchID),
    FOREIGN KEY (ISBN) REFERENCES Book(ISBN)
)