CREATE DATABASE client;

-- Use Database client
USE client;

-- Client Table
CREATE TABLE IF NOT EXISTS Client (
    ClientID INTEGER PRIMARY KEY,
    Name VARCHAR(255),
    Location VARCHAR(255)
);

-- Manager Table
CREATE TABLE IF NOT EXISTS Manager (
    ManagerID INTEGER PRIMARY KEY,
    Name VARCHAR(255),
    Location VARCHAR(255)
);

-- Contract Table
CREATE TABLE IF NOT EXISTS Contract (
    ContractID INTEGER PRIMARY KEY,
    EstimatedCost DECIMAL(10, 2),
    CompletionDate DATE,
    ManagerID INTEGER,
    ClientID INTEGER,
    FOREIGN KEY (ManagerID) REFERENCES Manager(ManagerID),
    FOREIGN KEY (ClientID) REFERENCES Client(ClientID)
);

-- Staff Table
CREATE TABLE IF NOT EXISTS Staff (
    StaffID INTEGER PRIMARY KEY,
    Name VARCHAR(255),
    Location VARCHAR(255),
    ManagerID INTEGER,
    FOREIGN KEY (ManagerID) REFERENCES Manager(ManagerID)
);
