CREATE DATABASE patient;

USE patient;

-- Secretary Table
CREATE TABLE IF NOT EXISTS Secretary (
    SecretaryID INTEGER PRIMARY KEY,
    Name VARCHAR(255)
);

-- Doctor Table
CREATE TABLE IF NOT EXISTS Doctor (
    DoctorID INTEGER PRIMARY KEY,
    SecretaryID INTEGER,
    FOREIGN KEY (SecretaryID) REFERENCES Secretary(SecretaryID)
);

-- Patient Table
CREATE TABLE IF NOT EXISTS Patient (
    PatientID INTEGER PRIMARY KEY,
    Name VARCHAR(255),
    DOB DATE,
    Address VARCHAR(255)
);

-- Prescription Table
CREATE TABLE IF NOT EXISTS Prescription (
    PrescriptionID INTEGER PRIMARY KEY,
    Drug VARCHAR(255),
    Date DATE, 
    Dosage VARCHAR(255),
    DoctorID INTEGER,
    PatientID INTEGER,
    FOREIGN KEY(DoctorID) REFERENCES Doctor(DoctorID),
    FOREIGN KEY(PatientID) REFERENCES Patient(PatientID)
);