CREATE DATABASE doctor;

USE doctor;

-- Secretary Table
CREATE TABLE IF NOT EXISTS Secretary (
    SecretaryID INTEGER PRIMARY KEY,
    Name VARCHAR(255)
);

-- DOctor Table
CREATE TABLE IF NOT EXISTS Doctor (
    DoctorID INTEGER PRIMARY KEY,
    Name VARCHAR(255),
    SecretaryID INTEGER,
    FOREIGN KEY(SecretaryID) REFERENCES Secretary(SecretaryID)
);

-- Patient Table
CREATE TABLE IF NOT EXISTS Patient (
    PatientID INTEGER PRIMARY KEY,
    Name VARCHAR(255),
    DOB DATE,
    Address VARCHAR(255),
    DoctorID INTEGER,
    FOREIGN KEY (DoctorID) REFERENCES Doctor(DoctorID)
);

-- Prescription Table
CREATE TABLE IF NOT EXISTS Prescription (
    PrescriptionID INTEGER PRIMARY KEY,
    Drug VARCHAR(255),
    Date DATE, 
    Dosage VARCHAR(255),
    PatientID INTEGER,
    FOREIGN KEY(PatientID) REFERENCES Patient(PatientID)
);